#!/usr/bin/env python
import sys

def main():
    """
    Process numbers and print the mod.
    """
    total = 0
    with open('input.txt') as f:
        for line in f.readlines():
            line.strip('\n')
            total += int(process_number(int(line)))

    print(total)
def process_number(value):
    return (value /3 ) - 2


if __name__ == '__main__':
    sys.exit(main())

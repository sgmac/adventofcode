package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

const filename = "input.txt"

func main() {

	mods, err := readfile(filename)
	if err != nil {
		log.Fatal(err)
		fmt.Println(mods)
	}

	var total int

	for _, v := range mods {
		total += computeFuel(v)
	}
	fmt.Println(total)
}

func readfile(file string) ([]int, error) {

	mods := make([]int, 0)
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		value, _ := strconv.Atoi(scanner.Text())
		if err != nil {
			return nil, err
		}

		mods = append(mods, value)
	}

	return mods, nil
}

func computeFuel(module int) int {
	fuelRequirement := (module / 3) - 2
	return fuelRequirement
}

#!/usr/bin/env ruby

total = 0
STDIN.read.split("\n").each do |v|
  v = (v.to_i / 3) -2 
  total += v
end
puts total

package main

import (
	"fmt"
	"log"
	"strconv"
	"strings"
)

func ar2str(initArray []string) []int {
	endArray := make([]int, 0)
	for _, v := range initArray {
		n, err := strconv.Atoi(v)
		if err != nil {
			log.Fatal(err)
		}
		endArray = append(endArray, n)
	}

	return endArray

}
func main() {

	input := "1,12,2,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,10,19,1,19,6,23,2,23,13,27,1,27,5,31,2,31,10,35,1,9,35,39,1,39,9,43,2,9,43,47,1,5,47,51,2,13,51,55,1,55,9,59,2,6,59,63,1,63,5,67,1,10,67,71,1,71,10,75,2,75,13,79,2,79,13,83,1,5,83,87,1,87,6,91,2,91,13,95,1,5,95,99,1,99,2,103,1,103,6,0,99,2,14,0,0"

	data := strings.Split(input, ",")

	newData := ar2str(data)

loop:
	for pos := range newData {
		var startOpCode int
		if pos == 0 || (pos%4) == 0 {
			startOpCode = newData[pos]
			if startOpCode == 99 {
				fmt.Println("halt execution")
				break loop
			}

			readPosA := newData[pos+1]
			readPosB := newData[pos+2]
			readPosS := newData[pos+3]

			valuePosA := newData[readPosA]
			valuePosB := newData[readPosB]
			valueStore := newData[readPosS]

			switch startOpCode {
			case 1:

				valueA := valuePosA
				valueB := valuePosB
				newData[valueStore] = valueA + valueB

			case 2:
				valueA := valuePosA
				valueB := valuePosB
				newData[valueStore] = valueA * valueB

			}
		}

	}
	fmt.Println(newData)
}
